DROP SCHEMA IF EXISTS public CASCADE;

CREATE SCHEMA public
    AUTHORIZATION postgres;

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';

SET default_tablespace = '';

SET default_with_oids = false;

CREATE TYPE public.type_ing AS ENUM (
    'sauce',
    'other'
);

CREATE TYPE public.type_pizza AS ENUM (
    'inMenu',
    'custom'
);

CREATE TYPE public.order_status AS ENUM (
    'accept',
    'decline',
    'pending',
    'basket'
);

CREATE SEQUENCE public.city_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER TABLE public.city_id_seq OWNER TO postgres;

CREATE TABLE public.city (
    id_city integer DEFAULT nextval('public.city_id_seq'::regclass) NOT NULL,
    name_city character varying(100) NOT NULL
);


ALTER TABLE public.city OWNER TO postgres;

CREATE SEQUENCE public.dish_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.dish_id_seq OWNER TO postgres;

CREATE TABLE public.dish (
    id_dish integer DEFAULT nextval('public.dish_id_seq'::regclass) NOT NULL,
    name_dish character varying(100) NOT NULL,
    price double precision NOT NULL,
    weight character varying(10) NOT NULL,
    image character varying(40) NOT NULL,    
    id_category integer NOT NULL
);


ALTER TABLE public.dish OWNER TO postgres;

CREATE SEQUENCE public.category_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.category_id_seq OWNER TO postgres;

CREATE TABLE public.category (
    id_category integer DEFAULT nextval('public.category_id_seq'::regclass) NOT NULL,
    name_category character varying(40) NOT NULL
);

ALTER TABLE public.category OWNER TO postgres;

CREATE SEQUENCE public.ingredient_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ingredient_id_seq OWNER TO postgres;

CREATE TABLE public.ingredient (
    id_ingredient integer DEFAULT nextval('public.ingredient_id_seq'::regclass) NOT NULL,
    name_ingredient character varying(50) NOT NULL,
    weight character varying(10) NOT NULL,
    price double precision NOT NULL,
    image character varying(100) NOT NULL,
    type public.type_ing NOT NULL
);

ALTER TABLE public.ingredient OWNER TO postgres;

CREATE SEQUENCE public.order_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.order_id_seq OWNER TO postgres;

CREATE TABLE public.order (
    id_order integer DEFAULT nextval('public.order_id_seq'::regclass) NOT NULL,
    id_user integer NOT NULL,
    ammount double precision NOT NULL,
    id_city integer NOT NULL,
    address character varying(100) NOT NULL,
    date timestamp with time zone NOT NULL DEFAULT Now(),
    dishes integer[] NOT NULL,
    pizzas integer[] NOT NULL,
    status public.order_status DEFAULT 'pending'
);


ALTER TABLE public.order OWNER TO postgres;

CREATE SEQUENCE public.pizza_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pizza_id_seq OWNER TO postgres;

CREATE TABLE public.pizza (
    id_pizza integer DEFAULT nextval('public.pizza_id_seq'::regclass) NOT NULL,
    name_pizza character varying(40) NOT NULL,
    size integer NOT NULL,
    weight character varying(10) NOT NULL,
    price double precision NOT NULL,
    image character varying(40) NOT NULL,
    ingredients integer[] NOT NULL,
    type public.type_pizza NOT NULL
);


ALTER TABLE public.pizza OWNER TO postgres;

CREATE SEQUENCE public.user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_id_seq OWNER TO postgres;

CREATE TABLE public.user (
    id_user integer DEFAULT nextval('public.user_id_seq'::regclass) NOT NULL,
    name_user character varying(40) NOT NULL,
    surname_user character varying(40) NOT NULL,
    phone character varying(20) NOT NULL,
    email character varying(40) NOT NULL,
    token character varying(120) NOT NULL,
    role character varying(20) NOT NULL DEFAULT 'user'
    
);

ALTER TABLE public.user OWNER TO postgres;


ALTER TABLE ONLY public.city
    ADD CONSTRAINT city_pkey PRIMARY KEY (id_city);

ALTER TABLE ONLY public.dish
    ADD CONSTRAINT dish_pkey PRIMARY KEY (id_dish);

ALTER TABLE ONLY public.category
    ADD CONSTRAINT category_pkey PRIMARY KEY (id_category);

ALTER TABLE ONLY public.ingredient
    ADD CONSTRAINT ingredient_pkey PRIMARY KEY (id_ingredient);

ALTER TABLE ONLY public.order
    ADD CONSTRAINT order_pkey PRIMARY KEY (id_order);

ALTER TABLE ONLY public.pizza
    ADD CONSTRAINT pizza_pkey PRIMARY KEY (id_pizza);

ALTER TABLE ONLY public.user
    ADD CONSTRAINT user_pkey PRIMARY KEY (id_user);


ALTER TABLE ONLY public.dish
    ADD CONSTRAINT "dish category" FOREIGN KEY (id_category) REFERENCES public.category(id_category);

ALTER TABLE ONLY public.order
    ADD CONSTRAINT "order user" FOREIGN KEY (id_user) REFERENCES public.user(id_user);

ALTER TABLE ONLY public.order
    ADD CONSTRAINT "order city" FOREIGN KEY (id_city) REFERENCES public.city(id_city);


--mock

INSERT INTO public.city ("name_city") VALUES ('Moscow');
INSERT INTO public.city ("name_city") VALUES ('Kiev');
INSERT INTO public.city ("name_city") VALUES ('London');

INSERT INTO public.category ("name_category") VALUES ('Drink');
INSERT INTO public.category ("name_category") VALUES ('Dessert');
INSERT INTO public.category ("name_category") VALUES ('Salad');

INSERT INTO public.dish ("name_dish", "price", "weight", "image", "id_category") VALUES ('Kola', 0.25, '0.3l', 'kola03l.png', 1);
INSERT INTO public.dish ("name_dish", "price", "weight", "image", "id_category") VALUES ('Kola', 0.5, '0.5l', 'kola05l.png', 1);
INSERT INTO public.dish ("name_dish", "price", "weight", "image", "id_category") VALUES ('Muffin with chocolate', 1, '200g', 'muffinChocolate200g.png', 2);
INSERT INTO public.dish ("name_dish", "price", "weight", "image", "id_category") VALUES ('Muffin with cherry', 1, '200g', 'muffinCherry200g.png', 2);
INSERT INTO public.dish ("name_dish", "price", "weight", "image", "id_category") VALUES ('Caesar', 15, '250g', 'caesar250g.png', 3);
INSERT INTO public.dish ("name_dish", "price", "weight", "image", "id_category") VALUES ('Nautical', 30, '250g', 'nautical250g.png', 3);

INSERT INTO public.ingredient ("name_ingredient", "weight", "price", "image", "type") VALUES ('Red sauce', '0g', 0, 'red_sauce.png', 'sauce');
INSERT INTO public.ingredient ("name_ingredient", "weight", "price", "image", "type") VALUES ('White sauce', '0g', 0, 'white_sauce.png', 'sauce');
INSERT INTO public.ingredient ("name_ingredient", "weight", "price", "image", "type") VALUES ('Sliced Pepperoni', '20g', 0.6, 'pepperoni.png', 'other');
INSERT INTO public.ingredient ("name_ingredient", "weight", "price", "image", "type") VALUES ('Pre-Cooked Bacon Pieces', '15g', 0.4, 'bacon.png', 'other');
INSERT INTO public.ingredient ("name_ingredient", "weight", "price", "image", "type") VALUES ('Cooked Beef Topping', '20g', 0.4, 'beef.png', 'other');
INSERT INTO public.ingredient ("name_ingredient", "weight", "price", "image", "type") VALUES ('Cooked Italian Sausage', '25g', 0.5, 'sausage.png', 'other');
INSERT INTO public.ingredient ("name_ingredient", "weight", "price", "image", "type") VALUES ('Cooked Italian Style Meatballs', '35g', 0.6, 'meatballs.png', 'other');
INSERT INTO public.ingredient ("name_ingredient", "weight", "price", "image", "type") VALUES ('Sliced Turkey Breast', '25g', 0.5, 'turkey.png', 'other');
INSERT INTO public.ingredient ("name_ingredient", "weight", "price", "image", "type") VALUES ('Sliced Hard Salami', '20g', 0.4, 'salami.png', 'other');
INSERT INTO public.ingredient ("name_ingredient", "weight", "price", "image", "type") VALUES ('Ham', '25g', 0.5, 'ham.png', 'other');
INSERT INTO public.ingredient ("name_ingredient", "weight", "price", "image", "type") VALUES ('Steak', '25g', 0.5, 'steak.png', 'other');
INSERT INTO public.ingredient ("name_ingredient", "weight", "price", "image", "type") VALUES ('Mushrooms', '20g', 0.2, 'mushrooms.png', 'other');
INSERT INTO public.ingredient ("name_ingredient", "weight", "price", "image", "type") VALUES ('Black Olives', '20g', 0.2, 'black-olives.png', 'other');
INSERT INTO public.ingredient ("name_ingredient", "weight", "price", "image", "type") VALUES ('Green Olives', '20g', 0.2, 'green-olives.png', 'other');
INSERT INTO public.ingredient ("name_ingredient", "weight", "price", "image", "type") VALUES ('Pineapple', '25g', 0.3, 'pineapple.png', 'other');
INSERT INTO public.ingredient ("name_ingredient", "weight", "price", "image", "type") VALUES ('Anchovy', '25g', 0.5, 'anchovy.png', 'other');
INSERT INTO public.ingredient ("name_ingredient", "weight", "price", "image", "type") VALUES ('Lobster', '25g', 0.7, 'lobster.png', 'other');
INSERT INTO public.ingredient ("name_ingredient", "weight", "price", "image", "type") VALUES ('Shrimps', '20g', 0.6, 'shrimps.png', 'other');
INSERT INTO public.ingredient ("name_ingredient", "weight", "price", "image", "type") VALUES ('Salmon', '30g', 0.7, 'salmon.png', 'other');
INSERT INTO public.ingredient ("name_ingredient", "weight", "price", "image", "type") VALUES ('Tuna', '25g', 0.6, 'Tuna.png', 'other');
INSERT INTO public.ingredient ("name_ingredient", "weight", "price", "image", "type") VALUES ('Squid', '25g', 0.5, 'squid.png', 'other');
INSERT INTO public.ingredient ("name_ingredient", "weight", "price", "image", "type") VALUES ('Sesame Seeds', '5g', 0.1, 'sesame.png', 'other');
INSERT INTO public.ingredient ("name_ingredient", "weight", "price", "image", "type") VALUES ('Boneless Wings', '25g', 0.5, 'boneless-wings.png', 'other');
INSERT INTO public.ingredient ("name_ingredient", "weight", "price", "image", "type") VALUES ('Buffalo Style Chicken Wings', '25g', 0.5, 'buffalo-wings.png', 'other');
INSERT INTO public.ingredient ("name_ingredient", "weight", "price", "image", "type") VALUES ('Traditional Feta', '25g', 0.4, 'feta.png', 'other');
INSERT INTO public.ingredient ("name_ingredient", "weight", "price", "image", "type") VALUES ('Parmesan Cheese', '25g', 0.4, 'parmesan.png', 'other');
INSERT INTO public.ingredient ("name_ingredient", "weight", "price", "image", "type") VALUES ('Mozzarella Cheese', '25g', 0.4, 'mozzarella.png', 'other');
INSERT INTO public.ingredient ("name_ingredient", "weight", "price", "image", "type") VALUES ('Asiago Cheese', '25g', 0.4, 'asiago.png', 'other');
INSERT INTO public.ingredient ("name_ingredient", "weight", "price", "image", "type") VALUES ('Cheddar Cheese', '25g', 0.4, 'cheddar.png', 'other');
INSERT INTO public.ingredient ("name_ingredient", "weight", "price", "image", "type") VALUES ('Croutons', '20g', 0.3, 'croutons.png', 'other');
INSERT INTO public.ingredient ("name_ingredient", "weight", "price", "image", "type") VALUES ('Cucumber', '20g', 0.2, 'cucumber.png', 'other');
INSERT INTO public.ingredient ("name_ingredient", "weight", "price", "image", "type") VALUES ('Tomato', '20g', 0.2, 'tomato.png', 'other');
INSERT INTO public.ingredient ("name_ingredient", "weight", "price", "image", "type") VALUES ('Mild Banana Peppers', '5g', 0.1, 'banana-peppers.png', 'other');
INSERT INTO public.ingredient ("name_ingredient", "weight", "price", "image", "type") VALUES ('Pepperoncini', '5g', 0.1, 'pepperoncini.png', 'other');
