const pgp = require('pg-promise')(/*options*/);

const cn = {
  host: 'localhost',
  port: 5432,
  database: 'siya',
  user: 'postgres',
  password: 'root'
};

module.exports = pgp(cn);
