const db = require('../../../db/connect-db');

// add new dynamic pizza for /add/pizza/dynamic request
function addDynamicPizza(req, res, next) {
    const weight = req.body.weight;
    const size = req.body.size;
    const price = req.body.price;
    const ingredients = req.body.ingredients;
    db.any(`INSERT INTO pizza(name_pizza, size, weight, price, image, ingredients, type) VALUES("Dynamic", ${size}, ${weight}, ${price}, "default-pizza.png", ${ingredients}, "custom")`)
        .then((data) => {
            return res.status(200).send(data);
        })
        .catch(() => {
            return res.status(400).send({message: 'Request can not be send'});
            });
}

module.exports = addDynamicPizza;