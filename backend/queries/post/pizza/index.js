const addPizza = require('./addPizza');
const addDynamicPizza = require('./addDynamicPizza');

module.exports = {
    addPizza,
    addDynamicPizza
};