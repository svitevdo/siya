const db = require('../../../db/connect-db');

// add new pizza for /add/pizza request
function addPizza(req, res, next) {
    const name = req.body.name;
    const weight = req.body.weight;
    const size = req.body.size;
    const image = req.body.image;
    const price = req.body.price;
    const ingredients = req.body.ingredients;
    db.any(`INSERT INTO pizza(name_pizza, size, weight, price, image, ingredients, type) VALUES(${name}, ${size}, ${weight}, ${price}, ${image}, ${ingredients}, 'inMenu')`)
        .then((data) => {
            return res.status(200).send(data);
        })
        .catch(() => {
            return res.status(400).send({message: 'Request can not be send'});
            });
}

module.exports = addPizza;