const db = require('../../../db/connect-db');

// add new dish for /add/dish request
function addDish(req, res, next) {
    const name = req.body.name;
    const price = req.body.price;
    const weight = req.body.weight;
    const image = req.body.image;
    const id_category = req.body.id_category;
    db.any(`INSERT INTO dish(name_dish, price, weight, image, id_category) VALUES(${name}, ${price}, ${weight}, ${image}, ${id_category})`)
        .then((data) => {
            return res.status(200).send(data);
        })
        .catch(() => {
            return res.status(400).send({message: 'Request can not be send'});
            });
}

module.exports = addDish;