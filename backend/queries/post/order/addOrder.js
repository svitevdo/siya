const db = require('../../../db/connect-db');

// add new order for /add/order request
function addOrder(req, res, next) {
    const id_user = req.body.id_user;
    const dishes = req.body.dishes;
    const pizzas = req.body.pizzas;
    db.any(`SELECT * FROM "order" WHERE id_user = ${id_user} AND status = 'basket'`)
      .then((data) => {
        if ( data.length === 0 ){
            db.any(`INSERT INTO "order"(id_user, 
                                ammount, 
                                id_city, 
                                address, 
                                dishes, 
                                pizzas, 
                                status) 
                VALUES(${id_user}, 0, 1, '', '{${dishes}}', '{${pizzas}}', 'basket')`)
            .then(() => {
                db.any(`SELECT * FROM "order" WHERE id_user = ${id_user} AND status = 'basket'`)
                    .then((data) => {
                    return res.status(200).send(data[0]);
                    });
            })
            .catch(() => {
                return res.status(400).send({message: 'Request can not be send'});
                });
        } else {
            return res.status(200).send(data[0]); 
        }
    })
}

module.exports = addOrder;