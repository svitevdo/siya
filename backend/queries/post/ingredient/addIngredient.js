const db = require('../../../db/connect-db');

// add new ingredient for /add/ingredient request
function addIngredient(req, res, next) {
    const name_ingredient = req.body.ingredient.name_ingredient;
    const weight = req.body.ingredient.weight;
    const price = req.body.ingredient.price;
    const image = req.body.ingredient.image;
    const type = req.body.ingredient.type;
    db.any(`INSERT INTO ingredient(name_ingredient, weight, price, image, type) VALUES(${name_ingredient},${weight},${price},${image},${type})`)
        .then((data) => {
            return res.status(200).send(data);
        })
        .catch(() => {
            return res.status(400).send({message: 'Request can not be send'});
            });
}

module.exports = addIngredient;