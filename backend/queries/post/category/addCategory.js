const db = require('../../../db/connect-db');

// add new category for /add/category request
function addCategory(req, res, next) {
    const category = req.body.category;
    db.any(`INSERT INTO category(name_category) VALUES(${category})`)
        .then((data) => {
            return res.status(200).send(data);
        })
        .catch(() => {
            return res.status(400).send({message: 'Request can not be send'});
            });
}

module.exports = addCategory;