const db = require('../../../db/connect-db');

// add new city for /add/city request
function addCity(req, res, next) {
    const city = req.body.city;
    db.any(`INSERT INTO city(name_city) VALUES(${city})`)
        .then((data) => {
            return res.status(200).send(data);
        })
        .catch(() => {
            return res.status(400).send({message: 'Request can not be send'});
            });
}

module.exports = addCity;