const db = require('../../../db/connect-db');

const validator = require('validator');
const bcrypt = require('bcrypt');

// Add new user  /sign/up
function userSignUp(req, res, next) {
  const userName = req.body.name;
  const userSurname = req.body.surname;
  const userEmail = req.body.email;
  const userPassword = req.body.password;
  const userPhone = req.body.phone;
  if (validUser(req.body)){
    db.any('SELECT * FROM "user" WHERE email = $1', userEmail)
      .then((data) => {
        if ( data.length === 0 ){
          bcrypt.hash(userPassword, 10)
            .then((hash) => {
              db.none('INSERT INTO "user"("name_user", "surname_user", phone, email, token) VALUES ($1, $2, $3, $4, $5)'
                ,[userName, userSurname, userPhone, userEmail, hash])
                .then(() => {
                  return res.status(200).send({message: "New user have been created", token: hash});
                })
                .catch(function (err) {
                  return next(err);
                });
            });
        }else{
          return res.status(400).send({message: "Email already exist"});
        }
      })
      .catch((err) => {
        return next(err);
      });
  }else{
    return res.status(400).send({message: "Invalid User"});
  }
}

function validUser(user){
  const validEmail = typeof user.email === 'string' &&
    user.email.trim() !== '' &&
    validator.isEmail(user.email);
  const validPassword = typeof user.password === 'string' &&
    user.password.trim() !== '' &&
    user.password.trim().length >= 6;
  const validName = typeof user.name === 'string' &&
    user.name.trim() !== '';
  const validSurname = typeof user.surname === 'string' &&
    user.surname.trim() !== '';
  const validPhone =  typeof user.phone === 'string' &&
  user.phone.trim() !== '' &&
  validator.isMobilePhone(user.phone);
  return validEmail && validPassword && validName && validSurname && validPhone;
}

module.exports = userSignUp;