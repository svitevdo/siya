const db = require('../../../db/connect-db');

const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');


// Login user /log/in
function userLogin(req, res, next) {
  const userEmail = req.body.email;
  const userPassword = req.body.password;
  db.any('SELECT * FROM "user" WHERE email = $1', userEmail)
    .then((data) => {
      if ( data.length !== 0 ){
        bcrypt.compare(userPassword, data[0].token)
          .then((result) => {
            if (result){
              let token = jwt.sign({ email: userEmail, role: data.role }, 'secret');
              return res.status(200).send({
                message: "Logged In",
                token: token,
              });
            }else{
              return res.status(400).send({message: "Passwords doesn't match"});
            }
          });
      }else{
        return res.status(400).send({message: "Email not exist"});
      }
    })
    .catch((err) => {
      return next(err);
    });
}
module.exports = userLogin;