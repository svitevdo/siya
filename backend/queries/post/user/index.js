const userLogIn = require('./userLogIn');
const userSignUp = require('./userSignUp');

module.exports = {
    userLogIn,
    userSignUp
};