const db = require('../../../db/connect-db');

// update order status on pending for /update/order/send request
function sendOrder(req, res, next) {
    const id_order = req.body.id_order;
    db.any(`UPDATE "order" SET status='pending' 
                        WHERE id_order=${id_order}`)
        .then((data) => {
            return res.status(200).send(data);
        })
        .catch(() => {
            return res.status(400).send({message: 'Request can not be send'});
            });
}

module.exports = sendOrder;