const db = require('../../../db/connect-db');

// update order status on decline for /update/order/decline request
function declineOrder(req, res, next) {
    const id_order = req.body.id_order;
    db.any(`UPDATE order SET status="decline") 
                        WHERE id_order=${id_order}`)
        .then((data) => {
            return res.status(200).send(data);
        })
        .catch(() => {
            return res.status(400).send({message: 'Request can not be send'});
            });
}

module.exports = declineOrder;