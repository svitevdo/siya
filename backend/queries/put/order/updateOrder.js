const db = require('../../../db/connect-db');

// update order for /update/order request
function updateOrder(req, res, next) {
    const id_order = req.body.id_order;
    const dishes = req.body.dishes;
    const pizzas = req.body.pizzas;
    db.any(`UPDATE "order" SET dishes='{${dishes}}',
                             pizzas='{${pizzas}}' 
                        WHERE id_order=${id_order}`)
        .then((data) => {
            return res.status(200).send(data);
        })
        .catch(() => {
            return res.status(400).send({message: 'Request can not be send'});
            });
}

module.exports = updateOrder;