const sendOrder = require('./sendOrder');
const acceptOrder = require('./acceptOrder');
const declineOrder = require('./declineOrder');
const updateOrder = require('./updateOrder');

module.exports = {
    sendOrder,
    acceptOrder,
    declineOrder,
    updateOrder
};