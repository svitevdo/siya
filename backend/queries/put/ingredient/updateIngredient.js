const db = require('../../../db/connect-db');

// update ingredient for /update/ingredient request
function updateIngredient(req, res, next) {
    const id = req.body.ingredient.id;
    const name_ingredient = req.body.ingredient.name_ingredient;
    const weight = req.body.ingredient.weight;
    const price = req.body.ingredient.price;
    const image = req.body.ingredient.image;
    const type = req.body.ingredient.type;
    db.any(`UPDATE category SET name_ingredient=${name_ingredient},
                                weight=${weight},
                                price=${price},
                                image=${image},
                                type=${type}) 
            WHERE id_ingredient=${id}`)
        .then((data) => {
            return res.status(200).send(data);
        })
        .catch(() => {
            return res.status(400).send({message: 'Request can not be send'});
            });
}

module.exports = updateIngredient;