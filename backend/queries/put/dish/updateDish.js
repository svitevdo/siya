const db = require('../../../db/connect-db');

// update dish for /update/dish request
function updateDish(req, res, next) {
    const id = req.body.id;
    const name = req.body.name;
    const price = req.body.price;
    const weight = req.body.weight;
    const image = req.body.image;
    const id_category = req.body.id_category;
    db.any(`UPDATE dish SET name_dish=${name},
                             price=${price},
                             weight=${weight},
                             image=${image},
                             id_category=${id_category}) 
                        WHERE id_dish=${id}`)
        .then((data) => {
            return res.status(200).send(data);
        })
        .catch(() => {
            return res.status(400).send({message: 'Request can not be send'});
            });
}

module.exports = updateDish;