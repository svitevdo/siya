const db = require('../../../db/connect-db');

// update category for /update/category request
function updateCategory(req, res, next) {
    const id = req.body.id;
    const category = req.body.category;
    db.any(`UPDATE category SET name_category=${category}) WHERE id_category=${id}`)
        .then((data) => {
            return res.status(200).send(data);
        })
        .catch(() => {
            return res.status(400).send({message: 'Request can not be send'});
            });
}

module.exports = updateCategory;