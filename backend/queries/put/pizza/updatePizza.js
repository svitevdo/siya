const db = require('../../../db/connect-db');

// update pizza for /update/pizza request
function updatePizza(req, res, next) {
    const id = req.body.id;
    const name = req.body.name;
    const size = req.body.size;
    const weight = req.body.weight;
    const price = req.body.price;
    const image = req.body.image;
    const ingredients = req.body.ingredients;
    db.any(`UPDATE pizza SET name_pizza=${name},
                             size=${size},
                             weight=${weight},
                             price=${price},
                             image=${image},
                             ingredients=${ingredients}) 
                        WHERE id_pizza=${id}`)
        .then((data) => {
            return res.status(200).send(data);
        })
        .catch(() => {
            return res.status(400).send({message: 'Request can not be send'});
            });
}

module.exports = updatePizza;