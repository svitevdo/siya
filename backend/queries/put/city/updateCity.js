const db = require('../../../db/connect-db');

// update city for /update/city request
function updateCity(req, res, next) {
    const id = req.body.id;
    const city = req.body.city;
    db.any(`UPDATE city SET name_city=${city}) WHERE id_city=${id}`)
        .then((data) => {
            return res.status(200).send(data);
        })
        .catch(() => {
            return res.status(400).send({message: 'Request can not be send'});
            });
}

module.exports = updateCity;