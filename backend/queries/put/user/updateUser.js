const db = require('../../../db/connect-db');

// Update info about user /update/user
function updateUser(req, res, next) {
  const id = req.body.user.id;
  const name = req.body.user.name;
  const surname = req.body.user.surname;
  const phone = req.body.user.phone;

  if (validUser(req.body.user )){
    db.any(`UPDATE user
                SET name_user=$1, surname_user=$2,
                phone=$3
                WHERE id_user=$4`, [name, surname, phone, id])
        .then((data) => {
        return res.status(200).send({
            message: "Your data changed"
        });
        })
        .catch((err) => {
        return next(err);
        });
    }
}

function validUser(user){
    const validName = typeof user.name === 'string' &&
        user.userName.trim() !== '';
    const validSurname = typeof user.surname === 'string' &&
        user.userSurname.trim() !== '';
    const validPhone =  typeof user.phone === 'string' &&
        user.phone.trim() !== '' &&
        validator.isMobilePhone(user.phone);

    return validEmail && validPassword && validName && validSurname && validPhone;
}

module.exports = updateUser;