const getAllDishes = require('./getAllDishes');
const getDishById = require('./getDishById');
const getDishesByCategory = require('./getDishesByCategory');

module.exports = {
    getAllDishes,
    getDishById,
    getDishesByCategory
};