const db = require('../../../db/connect-db');

// Get dish by id for /dish/:id request
function getDishById(req, res, next) {
   const id = req.params.id;
    db.any(`SELECT * FROM dish WHERE id_dish=${id}`)
        .then((data) => {
            return res.status(200).send(data);
        })
        .catch(() => {
            return res.status(400).send({message: 'Request can not be send'});
        });
}

module.exports = getDishById