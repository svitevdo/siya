const db = require('../../../db/connect-db');

// Get all dishes for /dishes request
function getAllDishes(req, res, next) {
    db.any('SELECT * FROM dish')
        .then((data) => {
            return res.status(200).send(data);
        })
        .catch(() => {
            return res.status(400).send({message: 'Request can not be send'});
        });
}

module.exports = getAllDishes;