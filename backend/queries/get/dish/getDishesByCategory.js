const db = require('../../../db/connect-db');

// Get dishes by category id for /dishes/:category request
function getDishesById(req, res, next) {
   const category = req.params.category;
    db.any(`SELECT * FROM dish WHERE id_category=${category}`)
        .then((data) => {
            return res.status(200).send(data);
        })
        .catch(() => {
            return res.status(400).send({message: 'Request can not be send'});
        });
}

module.exports = getDishesById