const db = require('../../../db/connect-db');

// Get all categories for /categories request
function getAllCategories(req, res, next) {
    db.any('SELECT * FROM category')
        .then((data) => {
            return res.status(200).send(data);
        })
        .catch(() => {
            return res.status(400).send({message: 'Request can not be send'});
        });
}

module.exports = getAllCategories;