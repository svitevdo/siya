const getAllPizzas = require('./getAllPizzas');
const getPizzaById = require('./getPizzaById');

module.exports = {
    getAllPizzas,
    getPizzaById
};