const db = require('../../../db/connect-db');

// Get all pizzas for /pizzas request
function getAllPizzas(req, res, next) {
    db.any(`SELECT
            id_pizza,
            name_pizza, 
            pizza.weight,
            pizza.image,
            size, 
            pizza.price,
            array_agg(distinct ingredient.name_ingredient) AS ingredients
            FROM pizza 
            INNER JOIN ingredient ON ingredient.id_ingredient = ANY(pizza.ingredients)
            WHERE pizza.type='inMenu'
            GROUP BY pizza.id_pizza`)
        .then((data) => {
            return res.status(200).send(data);
        })
        .catch(() => {
            return res.status(400).send({message: 'Request can not be send'});
        });
}

module.exports = getAllPizzas;