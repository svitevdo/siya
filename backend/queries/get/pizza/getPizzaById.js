const db = require('../../../db/connect-db');

// Get pizza by id for /pizzabyid request
function getPizzaById(req, res, next) {
   const id = req.params.id;
    db.any(`SELECT * FROM pizza WHERE id_pizza=${id}`)
        .then((data) => {
            return res.status(200).send(data);
        })
        .catch(() => {
            return res.status(400).send({message: 'Request can not be send'});
        });
}

module.exports = getPizzaById