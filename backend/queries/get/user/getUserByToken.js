const db = require('../../../db/connect-db');

const jwt = require('jsonwebtoken');

// Get user information by token  /user
function getUserByToken(req, res, next) {
  const token = req.params.token;
  let tokenVerified = jwt.verify(token, 'secret');
  const query = `SELECT * FROM "user" WHERE email = \'${tokenVerified.email}\'`;
  db.one(query)
    .then((data) => {
      return res.status(200).send(data);
    })
    .catch((err) => {
      return next(err);
    });
}

module.exports = getUserByToken;