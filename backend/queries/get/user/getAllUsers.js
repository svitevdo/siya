const db = require('../../../db/connect-db');

// Get users(except current) with some filters for /admin/users request
function getAllUsers (req, res) {
  const userId = req.query.userId;
  const numberOfUsers = req.query.numberOfUsers;
  const email = req.query.email;
  const phoneNumber = req.query.phoneNumber;
  let sortBy = '';
  switch (req.query.sortBy){
    case 'id':
      sortBy = 'users.id_user DESC';
      break;
    case 'first-name':
      sortBy = 'users.name';
      break;
    case 'last-name':
      sortBy = 'users.surname';
      break;
    case 'email':
      sortBy = 'users.email';
      break;
    default:
      sortBy = 'users.id_user';
      break;
  }
  db.any(`SELECT id_user, name_user, surname, email, role, phone FROM user
          WHERE id_user != ${userId}
          AND name_user ILIKE '%${nameInput}%'
          AND surname ILIKE '%${surnameInput}%'
          AND email ILIKE '%${email}%'
          AND phone ILIKE '%${phoneNumber}%'
          ORDER BY ${sortBy}
          LIMIT ${numberOfUsers}
          `)
    .then((data) => {
      db.any(`SELECT COUNT(*) FROM user
          WHERE id_user != ${userId}
          AND name_user ILIKE '%${nameInput}%'
          AND surname ILIKE '%${surnameInput}%'
          AND email ILIKE '%${email}%'
          AND phone ILIKE '%${phoneNumber}%'`)
        .then((count) => {
          return res.status(200).send({users:data, countOfUsers: count[0].count});
        })
        .catch(() => {
          return res.status(400).send({message: 'Can not get count of users in database'});
        });
    })
    .catch(() => {
      return res.status(400).send({message: 'Can not get users'});
    });
}

module.exports = getAllUsers;