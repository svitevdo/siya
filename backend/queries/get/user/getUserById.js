const db = require('../../../db/connect-db');

// Get user by id for /admin/user/:id request
function getUserById(req, res, next) {
  const idUser = parseInt(req.params.id);
  db.one(`SELECT * FROM users WHERE id_user = ${idUser}`)
    .then((data) => {
      return res.status(200).send({message: "Success", data: data});
    })
    .catch((err) => {
      return next(err);
    });
}

module.exports = getUserById;