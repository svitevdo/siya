const getAllUsers = require('./getAllUsers');
const getUserById = require('./getUserById');
const getUserByToken = require('./getUserByToken');

module.exports = {
    getAllUsers,
    getUserById,
    getUserByToken
};