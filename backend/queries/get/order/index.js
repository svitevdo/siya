const getAllOrders = require('./getAllOrders');
const getOrderById = require('./getOrderById');
const getAllUserOrders = require('./getAllUserOrders');

module.exports = {
    getAllOrders,
    getOrderById,
    getAllUserOrders
};