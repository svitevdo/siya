const db = require('../../../db/connect-db');

// Get orders by category id for /order/:user request
function getAllUserOrders(req, res, next) {
   const user = req.params.user;
    db.any(`SELECT * FROM order WHERE id_user=${user}`)
        .then((data) => {
            return res.status(200).send(data);
        })
        .catch(() => {
            return res.status(400).send({message: 'Request can not be send'});
        });
}

module.exports = getAllUserOrders