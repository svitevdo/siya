const db = require('../../../db/connect-db');

// Get all orders for /orders request
function getAllOrders(req, res, next) {
    db.any('SELECT * FROM order')
        .then((data) => {
            return res.status(200).send(data);
        })
        .catch(() => {
            return res.status(400).send({message: 'Request can not be send'});
        });
}

module.exports = getAllOrders;