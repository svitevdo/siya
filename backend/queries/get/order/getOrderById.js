const db = require('../../../db/connect-db');

// Get order by id for /order/:id request
function getOrderById(req, res, next) {
   const id = req.params.id;
    db.any(`SELECT * FROM order WHERE id_order=${id}`)
        .then((data) => {
            return res.status(200).send(data);
        })
        .catch(() => {
            return res.status(400).send({message: 'Request can not be send'});
        });
}

module.exports = getOrderById