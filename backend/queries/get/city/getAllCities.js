const db = require('../../../db/connect-db');

// Get all cities for /cities request
function getAllCities(req, res, next) {
    db.any('SELECT * FROM city')
        .then((data) => {
            return res.status(200).send(data);
        })
        .catch(() => {
            return res.status(400).send({message: 'Request can not be send'});
        });
}

module.exports = getAllCities;