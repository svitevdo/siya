const db = require('../../../db/connect-db');

// Get all ingredients for /ingredients request
function getAllIngredients(req, res, next) {
    db.any('SELECT * FROM ingredient')
        .then((data) => {
            return res.status(200).send(data);
        })
        .catch(() => {
            return res.status(400).send({message: 'Request can not be send'});
        });
}

module.exports = getAllIngredients;