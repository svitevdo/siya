const db = require('../../../db/connect-db');

// delete dish by id for /delete/dish request
function deleteDish(req, res, next) {
    const id = req.body.id;
    db.any(`DELETE FROM dish WHERE id_dish=${id}`)
        .then((data) => {
            return res.status(200).send(data);
        })
        .catch(() => {
            return res.status(400).send({message: 'Request can not be send'});
            });
}

module.exports = deleteDish;