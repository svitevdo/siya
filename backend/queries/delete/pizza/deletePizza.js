const db = require('../../../db/connect-db');

// delete pizza by id for /delete/pizza request
function deletePizza(req, res, next) {
    const id = req.body.id;
    db.any(`DELETE FROM pizza WHERE id_pizza=${id}`)
        .then((data) => {
            return res.status(200).send(data);
        })
        .catch(() => {
            return res.status(400).send({message: 'Request can not be send'});
            });
}

module.exports = deletePizza;