const db = require('../db/connect-db');

// Get

const getCities = require('./get/city/index');
const getCategories = require('./get/category/index');
const getIngredients = require('./get/ingredient/index');
const getUsers = require('./get/user/index');
const getPizzas = require('./get/pizza/index');
const getDishes = require('./get/dish/index');
const getOrders = require('./get/order/index');

// Post

const postCities = require('./post/city/index');
const postCategory = require('./post/category/index');
const postIngredient = require('./post/ingredient/index');
const postUser = require('./post/user/index');
const postPizza = require('./post/pizza/index');
const postDish = require('./post/dish/index')
const postOrder = require('./post/order/index')

// Put

const putCities = require('./put/city/index');
const putCategory = require('./put/category/index');
const putIngredient = require('./put/ingredient/index');
const putUser = require('./put/user/index');
const putPizza = require('./put/pizza/index');
const putDish = require('./put/dish/index');
const putOrder = require('./put/order/index');

// Delete
const deletePizza = require('./delete/pizza/index');
const deleteDish = require('./delete/dish/index');


module.exports = {
  // Get
  
  getAllCities: getCities.getAllCities,
  getAllCategories: getCategories.getAllCategories,
  getAllIngredients: getIngredients.getAllIngredients,
  getAllUsers: getUsers.getAllUsers,
  getUserById: getUsers.getUserById,
  getUserByToken: getUsers.getUserByToken,
  getAllPizzas: getPizzas.getAllPizzas,
  getPizzaById: getPizzas.getPizzaById,
  getAllDishes: getDishes.getAllDishes,
  getDishById: getDishes.getDishById,
  getDishesByCategory: getDishes.getDishesByCategory,
  getAllOrders: getOrders.getAllOrders,
  getOrderById: getOrders.getOrderById,
  getAllUserOrders: getOrders.getAllUserOrders,

  // Post
  
  addCity: postCities.addCity,
  addCategory: postCategory.addCategory,
  addIngredient: postIngredient.addIngredient,
  userSignUp: postUser.userSignUp,
  userLogIn: postUser.userLogIn,
  addPizza: postPizza.addPizza,
  addDynamicPizza: postPizza.addDynamicPizza,
  addDish: postDish.addDish,
  addOrder: postOrder.addOrder,

  // Put

  updateCity: putCities.updateCity,
  updateCategory: putCategory.updateCategory,
  updateIngredient: putIngredient.updateIngredient,
  updateUser: putUser.updateUser,
  updatePizza: putPizza.updatePizza,
  updateDish: putDish.updateDish,
  sendOrder: putOrder.sendOrder,
  acceptOrder: putOrder.acceptOrder,
  declineOrder: putOrder.declineOrder,
  updateOrder: putOrder.updateOrder,

 // Delete
  deletePizza: deletePizza.deletePizza,
  deleteDish: deleteDish.deleteDish

};
