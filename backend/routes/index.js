const router = require('express').Router();
const queries = require('../queries/index');
const multer  = require('multer');

let storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, 'uploads/');
    },
    filename: function (req, file, cb) {
      cb(null, new Date().getTime() + file.originalname);
    }
});
  
const fileFilter = (req, file, cb) => {
    if(file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') {
        cb(null, true);
    } else {
        cb(null, false);
    }
};

let upload = multer({ 
    storage: storage, 
    limits: {
        fileSize: 2428800
    },
    fileFilter: fileFilter
})

// Get Queries

router.get('/api/cities', queries.getAllCities);
router.get('/api/categories', queries.getAllCategories);
router.get('/api/ingredients', queries.getAllIngredients);
router.get('/api/admin/users', queries.getAllUsers);
router.get('/api/admin/user/:id', queries.getUserById);
router.get('/api/user/:token', queries.getUserByToken);
router.get('/api/pizzas', queries.getAllPizzas);
router.get('/api/pizza/:id', queries.getPizzaById);
router.get('/api/dishes', queries.getAllDishes);
router.get('/api/dish/:id', queries.getDishById);
router.get('/api/dishes/:category', queries.getDishesByCategory);
router.get('/api/orders', queries.getAllOrders);
router.get('/api/order/:id', queries.getOrderById);
router.get('/api/order/:user', queries.getAllUserOrders);


//Post Queries

router.post('/api/add/city', queries.addCity);
router.post('/api/add/category', queries.addCategory);
router.post('/api/add/ingredient', upload.single('uploads'), queries.addIngredient);
router.post('/api/sign/up', queries.userSignUp);
router.post('/api/log/in', queries.userLogIn);
router.post('/api/add/pizza', upload.single('uploads'), queries.addPizza);
router.post('/api/add/pizza/dynamic', queries.addDynamicPizza);
router.post('/api/add/dish', upload.single('uploads'), queries.addDish);
router.post('/api/add/order', queries.addOrder);

//Put Queries

router.put('/api/update/city', queries.updateCity);
router.put('/api/update/category', queries.updateCategory);
router.put('/api/update/ingredient', upload.single('uploads'), queries.updateIngredient);
router.put('/api/update/user', queries.updateUser);
router.put('/api/update/pizza', upload.single('uploads'), queries.updatePizza);
router.put('/api/update/dish', upload.single('uploads'), queries.updateDish);
router.put('/api/update/order/send', queries.sendOrder);
router.put('/api/update/order/accept', queries.acceptOrder);
router.put('/api/update/order/decline', queries.declineOrder);
router.put('/api/update/order', queries.updateOrder)

//Delete Queries
router.delete('/api/delete/pizza', queries.deletePizza);
router.delete('/api/delete/dish', queries.deleteDish);

module.exports = router;
