import axios from 'axios';
import { ActionTypes } from '../constans/ActionTypes';

export const getCategories = () => new Promise((dispatch) => {
    axios.get('/api/categories')
        .then((res) => {
            dispatch({type: ActionTypes.GET_CATEGORIES_FULFILLED, payload:res.data})
        })
        .catch((err) => {
            dispatch({type: ActionTypes.GET_CATEGORIES_REJECTED, payload:err.response.data})
        }) ;
  });

export const getDishByCategory = (category) => new Promise((dispatch) => {
    axios.get(`/api/dishes/${category}`)
        .then((res) => {
            dispatch({type: ActionTypes.GET_DISHES_BY_CATEGORY_FULFILLED, payload:res.data})
        })
        .catch((err) => {
            dispatch({type: ActionTypes.GET_DISHES_BY_CATEGORY_REJECTED, payload:err.response.data})
        }) ;
});

export const getPizzas = () => new Promise((dispatch) => {
    axios.get('/api/pizzas')
        .then((res) => {
            dispatch({type: ActionTypes.GET_PIZZAS_FULFILLED, payload:res.data})
        })
        .catch((err) => {
            dispatch({type: ActionTypes.GET_PIZZAS_REJECTED, payload:err.response.data})
        }) ;
  });