import axios from 'axios';
import { ActionTypes } from '../constans/ActionTypes';

export const signUp = (user) => new Promise((dispatch) => {
    axios.post('/api/sign/up', user)
        .then((res) => {
            dispatch({type: ActionTypes.SIGN_UP_USER_FULFILLED, payload:res.data})
        })
        .catch((err) => {
            dispatch({type: ActionTypes.SIGN_UP_USER_REJECTED, payload:err.response.data})
        }) ;
  });

export const signIn = (user) => new Promise((dispatch) => {
    axios.post('/api/log/in', user)
        .then((res) => {
            dispatch({type: ActionTypes.SIGN_IN_USER_FULFILLED, payload:res.data})
        })
        .catch((err) => {
            dispatch({type: ActionTypes.SIGN_IN_USER_REJECTED, payload:err.response.data})
        }) ;
});

export const getUserByToken = (token) => new Promise((dispatch) => {
    axios.get(`/api/user/${token}`)
        .then((res) => {
            dispatch({type: ActionTypes.GET_USER_FULFILLED, payload:res.data})
        })
        .catch((err) => {
            dispatch({type: ActionTypes.GET_IN_USER_REJECTED, payload:err.response.data})
        }) ;
});