import axios from 'axios';
import { ActionTypes } from '../constans/ActionTypes';

export const addOrder = (data) => new Promise((dispatch) => {
    axios.post(`/api/add/order`, data)
        .then((res) => {
            dispatch({type: ActionTypes.ADD_ORDER_FULFILLED, payload:res.data})
        })
        .catch((err) => {
            dispatch({type: ActionTypes.ADD_ORDER_REJECTED, payload:err.response.data})
        }) ;
});

export const updateOrder = (data) => new Promise((dispatch) => {
    axios.put(`/api/update/order`, data)
        .then((res) => {
            dispatch({type: ActionTypes.UPDATE_ORDER_FULFILLED, payload:res.data})
        })
        .catch((err) => {
            dispatch({type: ActionTypes.UPDATE_ORDER_REJECTED, payload:err.response.data})
        }) ;
});

export const sendOrder = (data) => new Promise((dispatch) => {
    axios.put(`/api/update/order/send`, {id_order: data.id_order})
        .then((res) => {
            dispatch({type: ActionTypes.SEND_ORDER_FULFILLED, payload:res.data})
        })
        .catch((err) => {
            dispatch({type: ActionTypes.SEND_ORDER_REJECTED, payload:err.response.data})
        }) ;
});