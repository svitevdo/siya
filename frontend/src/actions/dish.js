import axios from 'axios';
import { ActionTypes } from '../constans/ActionTypes';

export const getDishById= (id) => ((dispatch) => {
    dispatch({type: ActionTypes.GETTING_DISH, payload: true})
    axios.get(`/api/dish/${id}`)
        .then((res) => {
            dispatch({type: ActionTypes.GET_DISH_FULFILLED, payload:res.data})
            dispatch({type: ActionTypes.GETTING_DISH, payload: false})
        })
        .catch((err) => {
            dispatch({type: ActionTypes.GET_DISH_REJECTED, payload:err.response.data})
        }) ;
    });

export const getDishes = () => ((dispatch) => {
    dispatch({type: ActionTypes.GETTING_DISH, payload: true})
    axios.get(`/api/dishes`)
        .then((res) => {
            dispatch({type: ActionTypes.GET_DISHES_FULFILLED, payload:res.data})
            dispatch({type: ActionTypes.GETTING_DISH, payload: false})
        })
        .catch((err) => {
            dispatch({type: ActionTypes.GET_DISHES_REJECTED, payload:err.response.data})
        }) ;
    });
    
    