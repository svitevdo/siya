import { ActionTypes } from '../constans/ActionTypes';
import axios from 'axios';

export const getCities = () => ((dispatch) => {
    axios.get(`/api/cities`)
        .then((res) => {
            dispatch({type: ActionTypes.GET_CITIES_FULFILLED, payload:res.data})
        })
        .catch((err) => {
            dispatch({type: ActionTypes.GET_CITIES_REJECTED, payload:err.response.data})
        }) ;
    });
    