import React from 'react';
import ReactDOM from 'react-dom';
import './style/index.scss';
import App from './components/App/App';
import registerServiceWorker from './registerServiceWorker';
import {applyMiddleware, createStore} from 'redux';
import thunk from 'redux-thunk';
import promise from 'redux-promise'
import {Provider} from 'react-redux';
import reducer from './reducers/index';
import {composeWithDevTools} from 'redux-devtools-extension';

const store = createStore(reducer, composeWithDevTools(applyMiddleware(thunk, promise)));
ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>
  ,
document.getElementById('root'));

registerServiceWorker();
