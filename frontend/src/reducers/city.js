import { ActionTypes } from '../constans/ActionTypes';

export default function city(state = {cities: [] }, action) {
    switch (action.type) {
  
      case ActionTypes.GET_CITIES_REJECTED:
        return {
          ...state,
        };
      case ActionTypes.GET_CITIES_FULFILLED:
        return {
          ...state,
          cities: action.payload
        };
      default: return state;
    }
  }
  