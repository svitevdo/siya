import { ActionTypes } from '../constans/ActionTypes';

export default function users(state = {
    user:{},
    messageSignUp: "",
    messageSignIn: ""
  }, action) {
  
    switch (action.type) {
  
      case ActionTypes.SIGN_UP_USER_REJECTED:
        return {
          ...state,
          messageSignUp: action.payload.message
        };
      case ActionTypes.SIGN_UP_USER_FULFILLED:
        return {
          ...state,
          messageSignUp: action.payload.message,
          token: action.payload.token
        };
      case ActionTypes.SIGN_IN_USER_REJECTED:
        return {
          ...state,
          messageSignIn: action.payload.message
        };
      case ActionTypes.SIGN_IN_USER_FULFILLED:
        return {
          ...state,
          messageSignIn: action.payload.message,
          token: action.payload.token
        };
      case ActionTypes.GET_USER_REJECTED:
        return {
          ...state,
          
        };
      case ActionTypes.GET_USER_FULFILLED:
        return {
          ...state,
          user: action.payload
        };
      default: return state;
    }
  }
  