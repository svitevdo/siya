import { ActionTypes } from '../constans/ActionTypes';

export default function order(state = { 
    order: {
        dishes: [], 
        pizzas: []
    }
}, action) {
    switch (action.type) {
  
      case ActionTypes.ADD_DISH_TO_ORDER_FULFILLED:
        return {
          ...state,
          order: {
              ...state.order,
              dishes: state.order.dishes.concat([action.payload])
          }
        };
      case ActionTypes.DELETE_DISH_FROM_ORDER_FULFILLED:
        const dishes = Array.from(state.order.dishes);
        dishes.splice(state.order.dishes.indexOf(+action.payload), 1)
        return {
          ...state,
          order: {
            ...state.order,
            dishes
          }
        };
    case ActionTypes.ADD_PIZZA_TO_ORDER_FULFILLED:
        return {
          ...state,
          order: {
              ...state.order,
              pizzas: state.order.pizzas.concat([action.payload])
          }
        };
    case ActionTypes.DELETE_PIZZA_FROM_ORDER_FULFILLED:
        const pizzas = Array.from(state.order.pizzas);
        pizzas.splice(state.order.pizzas.indexOf(+action.payload), 1)
        return {
          ...state,
          order: {
            ...state.order,
            pizzas
          }
        };
    case ActionTypes.ADD_ORDER_FULFILLED:
        return {
          ...state,
          order: action.payload
        };
    case ActionTypes.ADD_ORDER_REJECTED:
        return {
          ...state,
        };
    case ActionTypes.UPDATE_ORDER_FULFILLED:
        return {
          ...state,
        };
    case ActionTypes.UPDATE_ORDER_REJECTED:
        return {
          ...state,
        };
    case ActionTypes.SEND_ORDER_FULFILLED:
        return {
          ...state,
          order: {
            dishes: [],
            pizzas: []
          }
        };
    case ActionTypes.SEND_ORDER_REJECTED:
        return {
          ...state,
        };
      default: return state;
    }
  }
  