import {combineReducers} from 'redux';

//import redusers
import users from './users';
import menu from './menu';
import order from './order';
import dish from './dish';
import city from './city'

export default combineReducers({
  //array redusers
    users,
    menu,
    order,
    dish,
    city
  }
);
