import { ActionTypes } from '../constans/ActionTypes';

export default function dish(state = {dish: {} }, action) {
    switch (action.type) {
  
      case ActionTypes.GET_DISHES_REJECTED:
        return {
          ...state,
        };
      case ActionTypes.GET_DISHES_FULFILLED:
        return {
          ...state,
          dishes: action.payload
        };
      case ActionTypes.GET_DISH_REJECTED:
        return {
          ...state,
        };
      case ActionTypes.GET_DISH_FULFILLED:
        return {
          ...state,
          dish: action.payload[0]
        };
      case ActionTypes.GETTING_DISH:
        return {
          ...state,
          getting: action.payload
        }
      default: return state;
    }
  }
  