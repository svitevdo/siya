import { ActionTypes } from '../constans/ActionTypes';

export default function menu(state = { categories: [], foods: []}, action) {
    switch (action.type) {
  
      case ActionTypes.GET_CATEGORIES_REJECTED:
        return {
          ...state,
        };
      case ActionTypes.GET_CATEGORIES_FULFILLED:
        return {
          ...state,
          categories: action.payload
        };
      case ActionTypes.GET_DISHES_BY_CATEGORY_FULFILLED:
        return {
          ...state,
          foods: action.payload
        };
      case ActionTypes.GET_DISHES_BY_CATEGORY_REJECTED:
        return {
          ...state,
        };
      case ActionTypes.GET_PIZZAS_REJECTED:
        return {
          ...state,
        };
      case ActionTypes.GET_PIZZAS_FULFILLED:
        return {
          ...state,
          foods: action.payload
        };
      default: return state;
    }
  }
  