import React, { Component } from 'react';
import { Modal, Button, Alert } from 'react-bootstrap';
import {connect} from 'react-redux';
import { signIn } from '../../actions/users';
import * as General from '../../constans/General';
import './SignIn.scss';

class SignIn extends Component {
  constructor(props, context) {
    super(props, context);

    this.state = {
        email: '',
        password: ''
    }
  }
  
  handleClose = () => {
    this.props.history.push('/home');
  }
  
  handleChange = (event) => {
    this.setState({
        [event.target.name]: event.target.value
    })
  }

  signIn = () => {
    const user = {...this.state};
    this.props.signIn(user)
  }

  renderAlert = () => {
      const messageSignIn = this.props.state.users.messageSignIn;
      if (messageSignIn === General.LOG_IN) {
          localStorage.setItem('token', this.props.state.users.token);
          this.handleClose();
        } else {
     return ( messageSignIn !== '' && <Alert bsStyle="danger">
                { messageSignIn }
            </Alert>);
    }
  }

  render() {
    const state = this.state;

    return (
        <Modal
            show={true}
            onHide={this.handleClose}
        >
          <Modal.Header closeButton>
          </Modal.Header>
          <Modal.Body>
            { this.renderAlert() }
            <form className="sign-up text-center" action="">
                <label className="form-group has-float-label">
                    <input 
                        className="form-control" 
                        type="email" 
                        name="email"
                        value={state.email} 
                        onChange={this.handleChange}
                        placeholder="email@example.com"/>
                    <span>Email</span>
                </label>
                <label className="form-group has-float-label">
                    <input 
                        className="form-control" 
                        type="password" 
                        name="password"
                        value={state.password} 
                        onChange={this.handleChange}
                        placeholder=""/>
                    <span>Password</span>
                </label>
                <Button className="green-button" onClick={this.signIn}>
                    Sign In
                </Button>
            </form>
          </Modal.Body>
        </Modal>
    );
  }
}
const mapStateToProps = (state) => {
    return  {
      state
    };
  };
  
  const mapDispatchToProps = (dispatch) => {
   return {
        signIn: (user) => {
            dispatch(signIn(user));
       }
   }
  };
  
export default connect(mapStateToProps,mapDispatchToProps) (SignIn);