import React, { Component } from 'react';
import { Modal, Button, Alert } from 'react-bootstrap';
import {connect} from 'react-redux';
import { signUp } from '../../actions/users';
import * as General from '../../constans/General';
import './SignUp.scss';

class SignUp extends Component {
  constructor(props, context) {
    super(props, context);

    this.state = {
        name: '',
        surname: '',
        phone: '',
        email: '',
        password: ''
    }
  }
  
  handleClose = () => {
    this.props.history.push('/home');
  }
  
  handleChange = (event) => {
    this.setState({
        [event.target.name]: event.target.value
    })
  }

  signUp = () => {
    const user = {...this.state};
    this.props.signUp(user)
  }

  renderAlert = () => {
      const messageSignUp = this.props.state.users.messageSignUp;
      if (messageSignUp === General.NEW_USER_CREATED) {
          localStorage.setItem('token', this.props.state.users.token);
          this.handleClose();
        } else {
     return ( messageSignUp !=='' && <Alert bsStyle="danger">
                { messageSignUp }
            </Alert>);
    }
  }

  render() {
    const state = this.state;

    return (
        <Modal
            show={true}
            onHide={this.handleClose}
        >
          <Modal.Header closeButton>
          </Modal.Header>
          <Modal.Body>
            { this.renderAlert() }
            <form className="sign-up text-center" action="">
                <label className="form-group has-float-label">
                    <input 
                        className="form-control" 
                        type="text"
                        name="name"
                        value={state.name} 
                        onChange={this.handleChange}
                        placeholder="John"/>
                    <span>Name</span>
                </label>
                <label className="form-group has-float-label">
                    <input 
                        className="form-control" 
                        type="text" 
                        name="surname"
                        value={state.surname} 
                        onChange={this.handleChange}
                        placeholder="Smith"/>
                    <span>Surname</span>
                </label>
                <label className="form-group has-float-label">
                    <input 
                        className="form-control" 
                        type="text" 
                        name="phone"
                        value={state.phone} 
                        onChange={this.handleChange}
                        placeholder="+380"/>
                    <span>Phone Number</span>
                </label>
                <label className="form-group has-float-label">
                    <input 
                        className="form-control" 
                        type="email" 
                        name="email"
                        value={state.email} 
                        onChange={this.handleChange}
                        placeholder="email@example.com"/>
                    <span>Email</span>
                </label>
                <label className="form-group has-float-label">
                    <input 
                        className="form-control" 
                        type="password" 
                        name="password"
                        value={state.password} 
                        onChange={this.handleChange}
                        placeholder=""/>
                    <span>Password</span>
                </label>
                <Button className="green-button" onClick={this.signUp}>
                    Sign Up
                </Button>
            </form>
          </Modal.Body>
        </Modal>
    );
  }
}
const mapStateToProps = (state) => {
    return  {
      state
    };
  };
  
  const mapDispatchToProps = (dispatch) => {
   return {
       signUp: (user) => {
            dispatch(signUp(user));
       }
   }
  };
  
export default connect(mapStateToProps,mapDispatchToProps) (SignUp);