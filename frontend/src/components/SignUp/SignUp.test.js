import SignUp from './SignUp'
import configureStore from 'redux-mock-store'
import { shallow } from 'enzyme'
import React from 'react';
import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

configure({ adapter: new Adapter() });

describe('SignUp',()=>{
    const initialState = {};
    const mockStore = configureStore();
    let store,container;

    beforeEach(()=>{
        store = mockStore(initialState);
        container = shallow(<SignUp store={store} /> );
    })

    it('render component', () => {
       expect(container.length).toEqual(1);
    });

    it('should pass input value to the handleChange handler', () => {
        const value = '2';
        const onChange = jest.fn();
        const wrapper = shallow(
            <input 
                        className="form-control" 
                        type="text"
                        name="name"
                        value='' 
                        onChange={onChange}
                        placeholder="John"/>
        );
    
        expect(wrapper).toMatchSnapshot();
    
            wrapper.find('input').simulate('change', {
            target: { value },
        });
    
        expect(onChange).toBeCalledWith( {
            target: { value }});
    });
})