import React, { Component } from 'react';
import { Navbar, Nav, NavItem, Button } from 'react-bootstrap';
import { NavLink } from "react-router-dom";
import logo from '../../images/SIYA.png';
import './Header.scss';

class Header extends Component {
  signOut = () => {
    localStorage.setItem('token', '');
  }
  render() {
    return (
        <Navbar className="header">
            <Navbar.Header>
                <Navbar.Brand>
                    <img src={logo} className="app-logo" alt="logo" />
                </Navbar.Brand>
                <Navbar.Toggle />
            </Navbar.Header>
            <Navbar.Collapse>
                <Nav className="icon-up-size" pullRight>
                    { !localStorage.getItem('token') && <NavItem eventKey={1} href="#">
                        <NavLink to="/sign/in" activeClassName="active">
                            <Button className="white-button">
                                Sign In
                            </Button>
                        </NavLink>
                    </NavItem> }
                    { !localStorage.getItem('token') && <NavItem eventKey={2} href="#">
                        <NavLink to="/sign/up" activeClassName="active">
                            <Button className="white-button">
                                Sign Up
                            </Button>
                        </NavLink>
                    </NavItem> }
                    { localStorage.getItem('token') && <NavItem eventKey={1} href="#">
                        <NavLink to="/basket" activeClassName="active">
                            <i className="fa fa-shopping-basket"/>
                        </NavLink>
                    </NavItem> }
                    { localStorage.getItem('token') && <NavItem eventKey={1} href="#">
                        <NavLink to="/home" onClick={() => this.signOut()} activeClassName="active">
                            <i className="fa fa-sign-out-alt"/>
                        </NavLink>
                    </NavItem> }
                    {/* <NavItem eventKey={2} href="#">
                        <i className="fa fa-user"/>
                    </NavItem> */}
                </Nav>
            </Navbar.Collapse>
        </Navbar>
    );
  }
}

export default Header;
