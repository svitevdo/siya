import React, { Component } from 'react';
import { Grid, Row, Button } from 'react-bootstrap';
import {connect} from 'react-redux';
import { getCategories, getDishByCategory, getPizzas } from '../../actions/menu';
import { addOrder, updateOrder } from '../../actions/order';
import { ActionTypes } from '../../constans/ActionTypes';
import MiniDish from './MiniDish/MiniDish';
import { getUserByToken } from '../../actions/users';
import './MainPage.scss';

class MainPage extends Component {
  constructor(props, context) {
    super(props, context);

    
    if (localStorage.getItem('token') && this.props.state.users.user !== {}){
        this.props.getUser(localStorage.getItem('token'))
    }
    
    this.props.getCategories();
    this.state = {
        choosenCategory: 0
    }
}

componentDidUpdate(prevProps) {
    if (this.props.state.order.order !== prevProps.state.order.order) {
        this.props.updateOrder(this.props.state.order.order);
    }
    if (this.props.state.users.user !== prevProps.state.users.user) {
        this.props.addOrder({
            id_user: this.props.state.users.user.id_user,
            dishes: [],
            pizzas: []
        });
        
    }
  }

  getDishByCategory = (id) => {
    this.props.getDishByCategory(id);
    this.setState({ choosenCategory: id});
  }
  
  getPizzas = () => {
    this.props.getPizzas();
    this.setState({ choosenCategory: 'pizza'});
  }

  addDishToOrder = (id) => {
    const {
        users
    } = this.props.state;
    if (users.token){
        this.props.addDishToOrder(id);
    } else {
        this.props.history.push('/sign/in');
    }
  }

  addPizzaToOrder = (id) => {
    const {
        users
    } = this.props.state;
    if (users.token){
        this.props.addPizzaToOrder(id);
    } else {
        this.props.history.push('/sign/in');
    }
  }

  render() {
    const {
        categories,
        foods
    } = this.props.state.menu;
    const {
        choosenCategory
    } = this.state;
    const listItems = categories.map((category) => 
        <Button 
            disabled={choosenCategory === category.id_category}
            onClick={() => this.getDishByCategory(category.id_category)} 
            className="menu-item">
            {category.name_category}
        </Button>
    );
    const listCards = foods.map((food) => 
        <MiniDish
            food={food}
            addDishToOrder={() => this.addDishToOrder(food.id_dish)}
            addPizzaToOrder={ () => this.addPizzaToOrder(food.id_pizza)}
            />
    )
    return (
        <Grid>
            <Row className="menu">
                {listItems}
                <Button 
                    disabled={choosenCategory === 'pizza'}
                    onClick={this.getPizzas} 
                    className="menu-item">
                    Pizzas
                </Button>
            </Row>
            { choosenCategory!==0 && <Row className="foods">
                {listCards}
            </Row> }
            
        </Grid>
    );
  }
}
const mapStateToProps = (state) => {
    return  {
      state
    };
  };
  
  const mapDispatchToProps = (dispatch) => {
    return {
        getCategories: () => {
            dispatch(getCategories());
        },
        getDishByCategory: (id) => {
            dispatch(getDishByCategory(id));
        },
        getPizzas: () => {
            dispatch(getPizzas());
        },
        addDishToOrder: (id) => {
            dispatch({type: ActionTypes.ADD_DISH_TO_ORDER_FULFILLED, payload: id});
        },
        addPizzaToOrder: (id) => {
            dispatch({type: ActionTypes.ADD_PIZZA_TO_ORDER_FULFILLED, payload: id});
        },
        addOrder: (data) => {
            dispatch(addOrder(data));
        },
        updateOrder: (data) => {
            dispatch(updateOrder(data));
        },
        getUser: (token) => {
            dispatch(getUserByToken(token));
        }
    }
  };
  
export default connect(mapStateToProps,mapDispatchToProps) (MainPage);