import React, { Component } from 'react';
import Flippy, { FrontSide, BackSide } from 'react-flippy';
import { Button } from 'react-bootstrap';
import './MiniDish.scss';

class MiniDish extends Component {
  constructor(props, context) {
    super(props, context);

    this.state = {
    }
  }

  render() {
    const {
        food
    } = this.props;
    // const ingredients = food.ingredients ? food.ingredients.map((ingredient) => 
    //                                             this.getIngridientById(ingredient)) : '';
    return (
        <Flippy
            flipOnHover={true} 
            flipDirection="horizontal" 
            ref={(r) => this.flippy = r} 
            style={{ width: '200px', 
                     height: '200px', 
                     'margin-right': '10px', 
                     display: 'inline-block'}} 
            >
            <FrontSide>
                <img 
                    className="food-image"
                    src={`${food.image}`} 
                    alt="dish"/>
            </FrontSide>
            <BackSide
                className="info-food">
                {food.name_dish && 
                    <div>
                        <h3>
                            {food.name_dish}
                        </h3>
                        <h4>
                            {food.weight}
                        </h4>
                        <h3>
                            {food.price + '$'}
                        </h3>
                        <Button 
                            className="icon-button"
                            onClick={this.props.addDishToOrder}>
                            <i className="fas fa-plus"/>
                        </Button>
                    </div>}
                {food.name_pizza && 
                    <div>
                        <h3>
                            {food.name_pizza}
                        </h3>
                        <h5>
                            {food.ingredients.join()}
                        </h5>
                        <h4>
                            {food.size}sm/{food.weight}
                        </h4>
                        <h3>
                            {food.price + '$'}
                            <Button 
                                className="icon-button"
                                onClick={this.props.addPizzaToOrder}>
                                <i className="fas fa-plus"/>
                            </Button>
                        </h3>
                    </div>}
            </BackSide>
        </Flippy>
    );
  }
}
  
export default MiniDish;