import React, { Component } from 'react';
import { Modal, Button, FormControl, FormGroup, ControlLabel } from 'react-bootstrap';
import {connect} from 'react-redux';
import { sendOrder, updateOrder } from '../../actions/order';
import './Order.scss';
import { getCities } from '../../actions/city';

class Order extends Component {
    constructor(props, context) {
        super(props, context);
    
        this.props.getCities();
        this.state = {
            city: '1',
            address: ''
        }
      }

    handleClose = () => {
        this.props.history.push('/basket');
    }

    handleChange = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        })
    }

    sendOrder = () => {
        let {
            order
        } = this.props.state.order;
        order.id_city = this.state.city;
        order.address = this.state.address;
        this.props.updateOrder(order);
        this.props.sendOrder(order);
        this.props.history.push('/home');
    }
    render() {
        const {
            state
        } = this;
        const citiesOptions = this.props.state.city.cities.map((city) => <option value={city.id_city}>{city.name_city}</option> )
        return (
            <Modal
                show={true}
                onHide={this.handleClose}
                >
                <Modal.Header closeButton/>
                <Modal.Body>
                <form className="order text-center" action="">
                <FormGroup controlId="city">
                    <ControlLabel>City</ControlLabel>
                    <FormControl componentClass="select" placeholder="select" name="city" onChange={this.handleChange}>
                        {citiesOptions}
                    </FormControl>
                </FormGroup>
                <label className="form-group has-float-label">
                    <input 
                        className="form-control" 
                        type="text" 
                        name="address"
                        value={state.address} 
                        onChange={this.handleChange}
                        placeholder="Smith"/>
                    <span>Address</span>
                </label>
                </form>
                    <div className="text-center">
                        <Button className="green-button" onClick={() => this.sendOrder()}>
                            Send order
                        </Button>
                    </div>
                </Modal.Body>
            </Modal>
    );
}
}
const mapStateToProps = (state) => {
    return  {
        state
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        updateOrder: (data) => {
            dispatch(updateOrder(data));
        },
        sendOrder: (data) => {
            dispatch(sendOrder(data));
        },
        getCities: () => {
            dispatch(getCities());
        }
    }
  };
  
export default connect(mapStateToProps,mapDispatchToProps) (Order);