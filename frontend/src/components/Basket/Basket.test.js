import Basket from './Basket'
import configureStore from 'redux-mock-store'
import { shallow } from 'enzyme'
import React from 'react';
import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import MiniDishOrder from './MiniDishOrder/MiniDishOrder';

configure({ adapter: new Adapter() });

describe('SignUp',()=>{
    const initialState = {};
    const mockStore = configureStore();
    let store,container;

    beforeEach(()=>{
        store = mockStore(initialState);
        container = shallow(<Basket store={store} /> );
    })

    it('render component', () => {
       expect(container.length).toEqual(1);
    });

    it('should failed', () => {
        const value = '2';
        const addDishToOrder = jest.fn();
        const addPizzaToOrder = jest.fn();

        const food = {
            'id_pizza': 1,
            'name_pizza': "Margarita",
            'weight': "250g",
            'image': "default-pizza.png",
            'size': 25,
            'price': 18,
            'ingredients': ["Red sauce",
                "Sliced Pepperoni", "White sauce"]
        }
        const wrapper = shallow(
            <MiniDishOrder
            count={0}
            addToOrder={addDishToOrder()}
            deleteFromOrder={addPizzaToOrder()}
            food={food}
            />
        );
    
        expect(wrapper).toMatchSnapshot();
    
            wrapper.find('button').simulate('onclick');
    
        expect(addDishToOrder).toBeCalledWith( {
            target: { value }});
    });
})