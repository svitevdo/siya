import React, { Component } from 'react';
import { Modal, Button } from 'react-bootstrap';
import {connect} from 'react-redux';
import { Link } from 'react-router-dom';
import { getDishes } from '../../actions/dish';
import { getPizzas } from '../../actions/menu';
import { updateOrder } from '../../actions/order';
import { ActionTypes } from '../../constans/ActionTypes';
import MiniDishOrder from './MiniDishOrder/MiniDishOrder'
import './Basket.scss';

class Basket extends Component {
  
componentWillMount(){
   this.props.getDishes();
   this.props.getPizzas();
}

componentDidUpdate(prevProps) {
    if (this.props.state.order.order !== prevProps.state.order.order) {
      this.props.updateOrder(this.props.state.order.order);
    }
}

handleClose = () => {
    this.props.history.push('/home');
}
  
getCounts(array) { 
    var result= {};
    for (var i = 0; i < array.length; ++i)
    {
        var a = array[i];
        if (result[a] !== undefined)
            ++result[a];
            else
            result[a] = 1;
    }
    return result;
}
 
getInfo () {
    const {
        dishes,
        pizzas
    } = this.props.state.order.order;
    const dishesCount= this.getCounts(dishes);
    const pizzasCount= this.getCounts(pizzas);
    
    let listOfOrder = [];
    for (let key in dishesCount) {
        const customDish = this.props.state.dish.dishes.find((dish) => {
            return dish.id_dish == key
        })
        listOfOrder.push(
            <MiniDishOrder
            food={customDish}
            count={dishesCount[key]}
            addToOrder={() => this.props.addDishToOrder(key)}
            deleteFromOrder={ () => this.props.deleteDishFromOrder(key)}
            />)
        }
    for (let key in pizzasCount) {
        const customPizza = this.props.state.menu.foods.find((pizza) => {
            return pizza.id_pizza == key
        })
        if (customPizza) {
            listOfOrder.push(
            <MiniDishOrder
                food={customPizza}
                count={pizzasCount[key]}
                addToOrder={() => this.props.addPizzaToOrder(key)}
                deleteFromOrder={ () => this.props.deletePizzaFromOrder(key)}
                />);
            }

    }

            return listOfOrder
        }

    isEmptyOrder = () => {
        const {
            dishes,
            pizzas
        } = this.props.state.order.order;
        return dishes.length === 0 && pizzas.length === 0
    }
    render() {
        return (
            <Modal
                show={true}
                onHide={this.handleClose}
                >
                <Modal.Header closeButton/>
                <Modal.Body>
                    {this.isEmptyOrder && <h2 className="text-center">Basket is empty</h2>}
                    {this.props.state.dish.getting === false && this.getInfo()}
                    <div className="text-center">
                        <Link to="/confirm/order" activeClassName="active">
                            <Button disabled={this.isEmptyOrder()} className="green-button">
                                Confirm order
                            </Button>
                        </Link>
                    </div>
                </Modal.Body>
            </Modal>
    );
}
}
const mapStateToProps = (state) => {
    return  {
        state
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        
        addDishToOrder: (id) => {
            dispatch({type: ActionTypes.ADD_DISH_TO_ORDER_FULFILLED, payload: id});
        },
        deleteDishFromOrder: (id) => {
            dispatch({type: ActionTypes.DELETE_DISH_FROM_ORDER_FULFILLED, payload: id});
        },
        addPizzaToOrder: (id) => {
            dispatch({type: ActionTypes.ADD_PIZZA_TO_ORDER_FULFILLED, payload: id});
        },
        deletePizzaFromOrder: (id) => {
            dispatch({type: ActionTypes.DELETE_PIZZA_FROM_ORDER_FULFILLED, payload: id});
        },
        getDishes: () => {
            dispatch(getDishes());
        },
        getPizzas: () => {
            dispatch(getPizzas());
        },
        updateOrder: (data) => {
            dispatch(updateOrder(data));
        }
    }
  };
  
export default connect(mapStateToProps,mapDispatchToProps) (Basket);