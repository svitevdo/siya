import React, { Component } from 'react';
import { Button, Row, Col } from 'react-bootstrap';
import './MiniDishOrder.scss';

class MiniDishOrder extends Component {
 
  render() {
      const {
          food
      } = this.props;
    return (
        <Row className="order">
            <Col md={3} xs={4}>
                <img 
                    className="food-image"
                    src={`${food.image}`} 
                    alt="dish"/>
            </Col>

            {food.name_dish && 
                <Col xs={4} md={4}  className="order-item" >
                    <h3>
                        {food.name_dish}
                    </h3>
                    <h4>
                        {food.weight}
                    </h4>
                    <h3>
                        {food.price + '$'}
                    </h3>
                </Col>}
            {food.name_pizza && 
                <Col xs={4} md={4}  className="order-item" >
                    <h3>
                      {food.name_pizza}
                    </h3>
                    <h5>
                        {food.ingredients.join()}
                    </h5>
                    <h4>
                        {food.size}sm/{food.weight}
                    </h4>
                    <h3>
                        {food.price + '$'}
                    </h3>
                </Col>}
                <Col xs={4} md={5} className="counter">
                    <Button 
                        className="icon-button"
                        onClick={this.props.deleteFromOrder}>
                        <i className="fas fa-minus"/>
                    </Button>
                    <h2 className="count">{this.props.count}</h2>
                    <Button 
                        className="icon-button"
                        onClick={this.props.addToOrder}>
                        <i className="fas fa-plus"/>
                    </Button>
                </Col>
            </Row>
    );
  }
}
  
export default MiniDishOrder;