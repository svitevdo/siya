import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Redirect, Switch} from "react-router-dom";
import './App.scss';
import Header from '../Header/Header';
import Footer from '../Footer/Footer';
import SignIn from '../SignIn/SignIn';
import SignUp from '../SignUp/SignUp'
import MainPage from '../MainPage/MainPage';
import Basket from '../Basket/Basket';
import Order from '../Order/Order';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Router>
        <div>
          <Header/>
          <Switch>
            <Route exact path='/sign/in' component={SignIn} />
            <Route exact path='/sign/up' component={SignUp} />
            <Route exact path='/basket' component={Basket} />
            <Route exact path='/confirm/order' component={Order} />

            <Route exact path='/home' component={MainPage} />

            <Redirect from="/" to="/home" />
          </Switch>
         </div>
      </Router>
        <Footer/>
      </div>
    );
  }
}

export default App;
