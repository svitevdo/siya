export const ActionTypes = {
    'SIGN_UP_USER_REJECTED': 'SIGN_UP_USER_REJECTED',
    'SIGN_UP_USER_FULFILLED': 'SIGN_UP_USER_FULFILLED',
    'SIGN_IN_USER_REJECTED': 'SIGN_IN_USER_REJECTED',
    'SIGN_IN_USER_FULFILLED': 'SIGN_IN_USER_FULFILLED',
    'GET_CATEGORIES_REJECTED': 'GET_CATEGORIES_REJECTED',
    'GET_CATEGORIES_FULFILLED': 'GET_CATEGORIES_FULFILLED',
    'GET_DISHES_BY_CATEGORY_REJECTED': 'GET_DISHES_BY_CATEGORY_REJECTED',
    'GET_DISHES_BY_CATEGORY_FULFILLED': 'GET_DISHES_BY_CATEGORY_FULFILLED',
    'GET_PIZZAS_REJECTED': 'GET_PIZZAS_REJECTED',
    'GET_PIZZAS_FULFILLED': 'GET_PIZZAS_FULFILLED',
    'ADD_DISH_TO_ORDER_FULFILLED': 'ADD_DISH_TO_ORDER_FULFILLED',
    'DELETE_DISH_FROM_ORDER_FULFILLED': 'DELETE_DISH_FROM_ORDER_FULFILLED',
    'ADD_PIZZA_TO_ORDER_FULFILLED': 'ADD_PIZZA_TO_ORDER_FULFILLED',
    'DELETE_PIZZA_FROM_ORDER_FULFILLED': 'DELETE_PIZZA_FROM_ORDER_FULFILLED',
    'ADD_ORDER_REJECTED': 'ADD_ORDER_REJECTED',
    'ADD_ORDER_FULFILLED': 'ADD_ORDER_FULFILLED',
    'UPDATE_ORDER_REJECTED': 'UPDATE_ORDER_REJECTED',
    'UPDATE_ORDER_FULFILLED': 'UPDATE_ORDER_FULFILLED',
    'GET_DISH_REJECTED': 'GET_DISH_REJECTED',
    'GET_DISH_FULFILLED': 'GET_DISH_FULFILLED',
    'GET_DISHES_REJECTED': 'GET_DISHES_REJECTED',
    'GET_DISHES_FULFILLED': 'GET_DISHES_FULFILLED',
    'GETTING_DISH': 'GETTING_DISH',
    'GET_CITIES_FULFILLED': 'GET_CITIES_FULFILLED',
    'GET_CITIES_REJECTED': 'GET_CITIES_REJECTED',
    'SEND_ORDER_FULFILLED': 'SEND_ORDER_FULFILLED',
    'SEND_ORDER_REJECTED': 'SEND_ORDER_REJECTED',
    'GET_USER_FULFILLED': 'GET_USER_FULFILLED',
    'GET_USER_REJECTED': 'GET_USER_REJECTED'

}